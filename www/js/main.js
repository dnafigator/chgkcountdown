var countdownSec = 0;
var countdownTen = false;
var countdownTimer = null;


function startCountdown(countdownSec, countdownTen )
{
	if (countdownTimer )
	{
		clearInterval ( countdownTimer );
		countdownTimer = null;
	}
	$('#countdown').removeClass ( 'red' );

	//_gaq.push(['_trackEvent', 'countdown', countdownTen ? 'commence_last' : 'commence', countdownSec]);
	//beep start
	$('#countdown').html ( countdownSec );
	countdownTimer = setInterval ( function(){
		countdownSec --;
		var sec = countdownSec;
		if ( 10 == countdownSec)
		{
			//beep ten
		}
		if ( countdownSec < 0 )
		{
			if ( countdownTen )
			{
				$('#countdown').addClass ( 'red' );
				//beep tick
				sec = 10 + countdownSec;
				if ( countdownSec < -10 )
				{
					//beep end
					clearInterval ( countdownTimer );
					sec = 0;
					countdownTimer = null;
					//_gaq.push(['_trackEvent', 'countdown', 'end']);
				}
			}
			else
			{
				$('#countdown').addClass ( 'red' );
				//beep end
				clearInterval ( countdownTimer );
				sec = 0;
				countdownTimer = null;
				//_gaq.push(['_trackEvent', 'countdown', 'end']);
			}
		}
		$('#countdown').html ( sec );
	}, 1000);
}

$(document).ready(function(){
	$('.startCountdown').on('click', function(){
		startCountdown ( $(this).attr ( 'ticks' ), $(this).attr ( 'ten' ) == 'yes' );
	});
		
	refreshTimer = setInterval ( function(){
		$.ajax("/get.php")
			.done(function(data){
				m = data.time.match ( '([0-9]+)_([0-9]+)' );
				if ( m && m.length >= 3 )
				{
					startCountdown ( parseInt(m[1]), 0 !== parseInt(m[2]) );
				}
			}).error(function(data){
				clearInterval ( refreshTimer );
			})
	}, 1000 );
		
});
